﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using NLog;
using NLog.Common;
using NLog.Config;
using NLog.Targets;
using ServiceStack;
using ServiceStack.Text;

namespace Nlog.Targets.Logstash
{
    [Target("Logstash")]
    public class LogstashTarget : TargetWithLayout
    {

        public string Host { get; set; }

        public int Port { get; set; }

        public string RequiredIndex { get; set; }

        public string User { get; set; }

        public string Password { get; set; }

        [ArrayParameter(typeof(Field), "field")]
        public IList<Field> Fields { get; set; }

        private IPEndPoint ip;

        private TcpClient _client;

        public LogstashTarget()
        {
            Fields = new List<Field>();
            _client = new TcpClient();
        }

        protected override void Write(LogEventInfo logEvent)
        {
            try
            {
                ip = new IPEndPoint(IPAddress.Parse(Host), Port);
            }
            catch (Exception)
            {
                var host = Dns.GetHostEntry(Host);
                ip = new IPEndPoint(host.AddressList[0], Port);
                
            }
            
            if (!_client.Connected)
            {
                _client = new TcpClient();
                _client.Connect(ip);

            }
            ThreadPool.QueueUserWorkItem(thread =>
            {               
                try
                {
                    
                    ServicePointManager.DefaultConnectionLimit = int.MaxValue;
                    var jsonContent = FormPayload(new[] {logEvent}).ToJson();
                    byte[] bodyEncoded = Encoding.UTF8.GetBytes(jsonContent);
                    StreamWriter sw = new StreamWriter(_client.GetStream());
                    sw.WriteLine(jsonContent);
                    sw.Flush();

                }
                catch (Exception ex)
                {
                    InternalLogger.Error(ex, "Failed to send to logstash");
                }
            });
        }

        private object FormPayload(IEnumerable<LogEventInfo> logEvents)
        {
            var jsonSerializer = new JsonStringSerializer();
            var payload = new List<object>();
            
            foreach (var logEvent in logEvents)
            {
                var document = new Dictionary<string, object>();

                if (logEvent.Exception != null)
                {
                    var ex = jsonSerializer.SerializeToString(logEvent.Exception);
                    var stacktrace = jsonSerializer.SerializeToString(logEvent.Exception.StackTrace);

                    document.Add("exception", ex);
                    document.Add("stackTrace", stacktrace);

                    if(logEvent.Exception.InnerException != null)
                    {
                        Exception innerMostException = logEvent.Exception.GetInnerMostException();
                        document.Add("innerMostException", jsonSerializer.SerializeToString(innerMostException));
                        document.Add("innerMostExceptionStackTrace", jsonSerializer.SerializeToString(innerMostException.StackTrace));
                    }
                }

                foreach (var field in Fields)
                {
                    string renderedField = field.Layout.Render(logEvent);

                    if (renderedField.IsNullOrEmpty() == false)
                    {
                        if (field.LayoutType == typeof(DateTime))
                        {
                            DateTime dateTime = DateTime.ParseExact(renderedField, field.InputDateTimeFormat, CultureInfo.InvariantCulture);
                            document[field.Name] = dateTime.ToString(field.OutputDateTimeFormat, CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            dynamic castedRenderedField = Convert.ChangeType(field.Layout.Render(logEvent), field.LayoutType, CultureInfo.InvariantCulture);
                            document[field.Name] = castedRenderedField;
                        }
                    }
                }
                document["requiredIndex"] = RequiredIndex;
                payload.Add(document);
            }

            return payload;
        }


    }
}
