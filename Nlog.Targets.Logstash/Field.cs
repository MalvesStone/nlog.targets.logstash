﻿using System;
using NLog.Config;
using NLog.Layouts;

namespace Nlog.Targets.Logstash
{
    [NLogConfigurationItem]
    public class Field
    {
        public Field()
        {
            LayoutType = typeof(string);
            InputDateTimeFormat = "yyyy-MM-dd HH:mm:ss.ffff";
            OutputDateTimeFormat = "o";
        }

        [RequiredParameter]
        public string Name { get; set; }

        [RequiredParameter]
        public Layout Layout { get; set; }

        public Type LayoutType { get; set; }

        public string InputDateTimeFormat { get; set; }

        public string OutputDateTimeFormat { get; set; }

        public override string ToString()
        {
            return $"Name: {Name}, LayoutType: {LayoutType}, Layout: {Layout}, InputDateTimeFormat: {InputDateTimeFormat}, OutputDateTimeFormat: {OutputDateTimeFormat}";
        }
    }
}