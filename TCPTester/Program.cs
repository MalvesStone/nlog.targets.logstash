﻿using System;
using Nlog.Targets.Logstash;
using NLog;

namespace TCPTester
{
    class Program
    {
        static void Main(string[] args)

        {
            ILogger logger = LogManager.GetCurrentClassLogger();

            try
            {
                throw new Exception("An exception");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                logger.Error(ex, "An error message");
            }

            logger.Info("Just a message");
            Console.ReadLine();
        }
    }
}
