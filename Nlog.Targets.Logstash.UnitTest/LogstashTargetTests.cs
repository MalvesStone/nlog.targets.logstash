﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using Xunit;

namespace Nlog.Targets.Logstash.UnitTest
{
    public class LogstashTargetTests
    {
        /// <summary>
        /// Class to be tested.
        /// </summary>
        private LogstashTarget _logstashTarget;

        /// <summary>
        /// Class used to support test private methods.
        /// </summary>
        private PrivateObject _privateLogstashTarget;

        public LogstashTargetTests()
        {
            this._logstashTarget = new LogstashTarget();
            this._privateLogstashTarget = new PrivateObject(this._logstashTarget);
        }

        [Fact]
        public void FormPayload_ShouldSerializeStringAsString()
        {
            /// Setup ///

            // Tested fields
            string hostname = "localhost";

            Field stringField = new Field() { Layout = "${event-properties:item=hostname}", LayoutType = Type.GetType("System.String"), Name = "hostname" };

            // Another field
            string requiredIndex = "index";

            // NLog config file target (setting as private object)
            IList<Field> fields = new List<Field>();
            fields.Add(stringField);
            this._privateLogstashTarget.SetProperty("Fields", fields);

            // Create a log info
            LogEventInfo logInfo = new LogEventInfo();
            logInfo.Level = LogLevel.Info;
            logInfo.Message = "logging with properties";
            logInfo.Properties.Add("hostname", hostname);
            logInfo.Properties.Add("requiredIndex", requiredIndex);

            // Method input
            IEnumerable<LogEventInfo> logEvents = new List<LogEventInfo>()
            {
                logInfo
            };


            /// Fact ///

            // Call method
            IEnumerable<object> payload = (IEnumerable<object>)this._privateLogstashTarget.Invoke("FormPayload", logEvents);

            // Cast response
            List<IDictionary<string, object>> castedPayload = new List<IDictionary<string, object>>();
            foreach (object logEvent in payload)
            {
                castedPayload.Add((IDictionary<string, object>)logEvent);
            }


            /// Asserts ///

            // Number of logevents in payload
            Xunit.Assert.Equal(logEvents.Count(), payload.Count());

            // Number of properties in payload (testing just the first logevent, because this test use just one)
            // TODO: iterate all loginfo comparing all properties
            Xunit.Assert.Equal(logEvents.First().Properties.Count(), castedPayload.First().Count());

            // Properties in payload (testing just the first logevent)
            Xunit.Assert.Equal(logInfo.Properties[stringField.Name], castedPayload.First()[stringField.Name]);
        }

        [Fact]
        public void FormPayload_ShouldSerializeNumberAsNumber()
        {
            /// Setup ///

            // Tested fields
            int id = 312;
            float amount = 3.13F;
            double anotherAmount = 3.14F;
            long negativeValue = -9999999999999;
            ulong unsignedValue = 999999999999999;

            Field intField = new Field() { Layout = "${event-properties:item=id}", LayoutType = Type.GetType("System.Int32"), Name = "id" };
            Field floatField = new Field() { Layout = "${event-properties:item=amount}", LayoutType = Type.GetType("System.Single"), Name = "amount" };
            Field doubleField = new Field() { Layout = "${event-properties:item=anotherAmount}", LayoutType = Type.GetType("System.Double"), Name = "anotherAmount" };
            Field longField = new Field() { Layout = "${event-properties:item=negativeValue}", LayoutType = Type.GetType("System.Int64"), Name = "negativeValue" };
            Field ulongField = new Field() { Layout = "${event-properties:item=unsignedValue}", LayoutType = Type.GetType("System.UInt64"), Name = "unsignedValue" };

            // Another field
            string requiredIndex = "index";

            // NLog config file target (setting as private object)
            IList<Field> fields = new List<Field>();
            fields.Add(intField);
            fields.Add(floatField);
            fields.Add(doubleField);
            fields.Add(longField);
            fields.Add(ulongField);
            this._privateLogstashTarget.SetProperty("Fields", fields);

            // Create a log info
            LogEventInfo logInfo = new LogEventInfo();
            logInfo.Level = LogLevel.Info;
            logInfo.Message = "logging with properties";
            logInfo.Properties.Add("id", id);
            logInfo.Properties.Add("amount", amount);
            logInfo.Properties.Add("anotherAmount", anotherAmount);
            logInfo.Properties.Add("negativeValue", negativeValue);
            logInfo.Properties.Add("unsignedValue", unsignedValue);
            logInfo.Properties.Add("requiredIndex", requiredIndex);

            // Method input
            IEnumerable<LogEventInfo> logEvents = new List<LogEventInfo>()
            {
                logInfo
            };

            /// Fact ///

            // Call method
            IEnumerable<object> payload = (IEnumerable<object>)this._privateLogstashTarget.Invoke("FormPayload", logEvents);

            // Cast response
            List<IDictionary<string, object>> castedPayload = new List<IDictionary<string, object>>();
            foreach (object logEvent in payload)
            {
                castedPayload.Add((IDictionary<string, object>)logEvent);
            }


            /// Asserts ///

            // Number of logevents in payload
            Xunit.Assert.Equal(logEvents.Count(), payload.Count());

            // Number of properties in payload (testing just the first logevent, because this test use just one)
            // TODO: iterate all loginfo comparing all properties
            Xunit.Assert.Equal(logEvents.First().Properties.Count(), castedPayload.First().Count());

            // Properties in payload (testing just the first logevent)
            Xunit.Assert.Equal(logInfo.Properties[intField.Name], castedPayload.First()[intField.Name]);
            Xunit.Assert.Equal(logInfo.Properties[floatField.Name], castedPayload.First()[floatField.Name]);
            Xunit.Assert.Equal((double)logInfo.Properties[doubleField.Name], (double)castedPayload.First()[doubleField.Name], 13); // Using precision to limit comparison
            Xunit.Assert.Equal(logInfo.Properties[longField.Name], castedPayload.First()[longField.Name]);
            Xunit.Assert.Equal(logInfo.Properties[ulongField.Name], castedPayload.First()[ulongField.Name]);
        }

        [Fact]
        public void FormPayload_ShouldSerializeDateTimeInSpecifiedFormat()
        {
            /// Setup ///

            // Tested fields
            DateTime dateTime = DateTime.UtcNow;

            Field customDateTimeField = new Field() { Layout = "${event-properties:item=anotherDate}", LayoutType = Type.GetType("System.DateTime"), Name = "anotherDate", InputDateTimeFormat = "MM/dd/yyyy HH:mm:ss", OutputDateTimeFormat = "yyyy-MM-dd HH:mm:ss" };

            // Another field
            string requiredIndex = "index";

            // NLog config file target (setting as private object)
            IList<Field> fields = new List<Field>();
            fields.Add(customDateTimeField);
            // nlog date time field will be automatically inserted by NLog framework
            this._privateLogstashTarget.SetProperty("Fields", fields);

            // Create a log info
            LogEventInfo logInfo = new LogEventInfo();
            logInfo.Level = LogLevel.Info;
            logInfo.Message = "logging with properties";
            logInfo.Properties.Add("anotherDate", dateTime);
            logInfo.Properties.Add("requiredIndex", requiredIndex);

            // Method input
            IEnumerable<LogEventInfo> logEvents = new List<LogEventInfo>()
            {
                logInfo
            };


            /// Fact ///

            // Call method
            IEnumerable<object> payload = (IEnumerable<object>)this._privateLogstashTarget.Invoke("FormPayload", logEvents);

            // Cast response
            List<IDictionary<string, object>> castedPayload = new List<IDictionary<string, object>>();
            foreach (object logEvent in payload)
            {
                castedPayload.Add((IDictionary<string, object>)logEvent);
            }


            /// Asserts ///

            // Number of logevents in payload
            Xunit.Assert.Equal(logEvents.Count(), payload.Count());

            // Number of properties in payload (testing just the first logevent, because this test use just one)
            // TODO: iterate all loginfo comparing all properties
            Xunit.Assert.Equal(logEvents.First().Properties.Count(), castedPayload.First().Count());

            // Properties in payload (testing just the first logevent)
            Xunit.Assert.Equal(dateTime.ToString(customDateTimeField.OutputDateTimeFormat), castedPayload.First()[customDateTimeField.Name]);
        }

        [Fact]
        public void FormPayload_ShouldSerializeDefaultNlogTimeInDefaultSpecifiedFormat()
        {
            /// Setup ///

            // Tested fields
            Field nlogDateTimeField = new Field() { Layout = "${longdate}", LayoutType = Type.GetType("System.DateTime"), Name = "longdate" };

            // Another field
            string requiredIndex = "index";

            // NLog config file target (setting as private object)
            IList<Field> fields = new List<Field>();
            fields.Add(nlogDateTimeField);
            this._privateLogstashTarget.SetProperty("Fields", fields);

            // Create a log info
            LogEventInfo logInfo = new LogEventInfo();
            logInfo.Level = LogLevel.Info;
            logInfo.Message = "logging with properties";
            // nlog date time field will be automatically inserted by NLog framework
            logInfo.Properties.Add("requiredIndex", requiredIndex);

            // Method input
            IEnumerable<LogEventInfo> logEvents = new List<LogEventInfo>()
            {
                logInfo
            };


            /// Fact ///

            // Call method
            IEnumerable<object> payload = (IEnumerable<object>)this._privateLogstashTarget.Invoke("FormPayload", logEvents);

            // Cast response
            List<IDictionary<string, object>> castedPayload = new List<IDictionary<string, object>>();
            foreach (object logEvent in payload)
            {
                castedPayload.Add((IDictionary<string, object>)logEvent);
            }


            /// Asserts ///

            // Number of logevents in payload
            Xunit.Assert.Equal(logEvents.Count(), payload.Count());

            // Number of properties in payload (testing just the first logevent, because this test use just one)
            // TODO: iterate all loginfo comparing all properties
            Xunit.Assert.Equal(logEvents.First().Properties.Count() + 1, castedPayload.First().Count()); // default date isn't in properties, then we expect +1

            // Properties in payload (testing just the first logevent)
            //Xunit.Assert.Equal(dateTime.ToString(customDateTimeField.OutputDateTimeFormat), castedPayload.First()[customDateTimeField.Name]);
            DateTime.ParseExact(castedPayload.First()[nlogDateTimeField.Name].ToString(), nlogDateTimeField.OutputDateTimeFormat, CultureInfo.InvariantCulture);
        }
    }
}
