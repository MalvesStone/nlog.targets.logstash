﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace LogstashTester
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime a = DateTime.UtcNow;
            Console.WriteLine(a.ToString("yyyy-MM-dd hh:mm:ss tt", CultureInfo.InvariantCulture));

            ILogger Logger = LogManager.GetLogger("MyLogger");

            LogEventInfo logEventInfo = new LogEventInfo()
            {
                Level = LogLevel.Info,
                Message = "testando",
            };

            logEventInfo.Properties.Add(new KeyValuePair<object, object>("SomeString", "Uma String"));
            logEventInfo.Properties.Add(new KeyValuePair<object, object>("SomeInt", 123));
            logEventInfo.Properties.Add(new KeyValuePair<object, object>("SomeFloat", 3.41F));
            logEventInfo.Properties.Add(new KeyValuePair<object, object>("SomeDouble", 4.64564));
            logEventInfo.Properties.Add(new KeyValuePair<object, object>("SomeNegative", -10324));
            logEventInfo.Properties.Add(new KeyValuePair<object, object>("SomeUnsigned", long.MaxValue));
            logEventInfo.Properties.Add(new KeyValuePair<object, object>("SomeDate", DateTime.Now.AddDays(12)));

            Logger.Log(logEventInfo);
        }
    }
}
